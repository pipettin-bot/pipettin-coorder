#include <ArduinoJson.h>  // Parse JSON messsages from the serial interface.

float variable1 = 10;
float variable2 = 20;

void setup() {

  // Initialize serial communications at 115200 bps.
  Serial.begin(115200);

  // Use "SerialUSB" instead of Serial to send messages if on an Arduino zero chip (e.g. the one in Pocket PCRs), 
  // otherwise no messages will be sent to the serial monitor.
  // https://forum.arduino.cc/t/i-cannot-get-the-serial-print-to-work-in-zero/571174/6
  // SerialUSB.begin(115200);

  while (!Serial) continue;

  send_hello();
}

void loop() {

  if(Serial){
    // Send alive signal.
    send_alive();
    
    // Echo incoming serial messages.
    // echo_serial();

    // Parse JSON messages
    parse_json_from_serial();
  }

  // Place the rest of your code here.
  // ...
  
}

// Send hello message.
void send_hello(){
  Serial.println("{\"method\":\"status\",\"data\":\"starting\"}");
}

// Send alive message if a second has elapsed.
float timestamp = 0;
void send_alive(){
  float now = millis();
  if(now > timestamp + 5000){
    Serial.println("{\"method\":\"status\",\"data\":\"ok\"}");
    timestamp = now;
  }
}

// Echo messages received through serial.
void echo_serial(){
  // Send data only when you receive data: https://gist.github.com/Protoneer/96db95bfb87c3befe46e
  int incomingByte = 0;    // for incoming serial data
  if (Serial.available() > 0) {
    while(Serial.available() > 0){
      // read the incoming byte:
      incomingByte = Serial.read();
      // say what you got:
      Serial.print((char)incomingByte);
    }
    // Serial.println();
  }
}

// Deserialize a JSON document with ArduinoJson.
// https://arduinojson.org/v6/example/parser/
void parse_json_from_serial() {
  // Check if the computer is transmitting.
  while(Serial.available()) {
    
    // Skip newline characters.
    // Peek on the next byte from the Serial interface,
    // and skip it if it is a newline or carriage return.
    int next_byte = Serial.peek();
    
    // Newline '\n' has code 10, carriage-return has code 13.
    if(next_byte == 10 || next_byte == 13){
      // Get and remove the NL/CR byte from the serial buffer.
      Serial.read();
    
      // Notes:
      // For comparisons, a character is enclosed in single quotes ('\n') not double quotes.
      // https://www.cs.cmu.edu/~pattis/15-1XX/common/handouts/ascii.html
      // https://forum.arduino.cc/t/recognizing-new-line-character/619160/2
      // https://forum.arduino.cc/t/how-to-check-cr-in-the-char-from-serial-read-function/180110/4
      
    // Parse JSON and call methods.
    } else {
      // Allocate the JSON document for the incoming message.
      // This one must be bigger than the sender's because it must store the strings.
      //
      // Inside the brackets, 200 is the RAM allocated to this document.
      // Don't forget to change this value to match your requirement.
      // Use https://arduinojson.org/v6/assistant to compute the capacity.
      StaticJsonDocument<300> doc;

      // StaticJsonObject allocates memory on the stack, it can be
      // replaced by DynamicJsonDocument which allocates in the heap.
      //
      // DynamicJsonDocument  doc(200);

      // Read the JSON document from the "link" serial port.
      // Note: JSON reading terminates when a valid JSON is received, not considering line breaks.
      // Adding them to the message will result in incomplete/empty errors from deserializeJson.
      DeserializationError err = deserializeJson(doc, Serial);

      // Check if parsing went well.
      if (err == DeserializationError::Ok){
        
        // If so, get the method's name.
        String method = doc["method"] | "none";
        
        // Allocate the JSON document for the response.
        StaticJsonDocument<200> response_doc;
        // Insert the ID or a default one if it is missing.
        bool has_id = doc.containsKey("id"); // true
        if(!has_id){
          response_doc["id"] = (int)-1;
        } else {
          response_doc["id"] = doc["id"];
        }

        // Then route the contents to the appropriate local function.
        if (method == "none"){
          response_doc["message"] = "No method selected";
        
        // Example method.
        } else if(method == "example"){
          // The following document is expected:
          // {"method": "example", "timestamp": 1234, "value": 687, "id": 10}
          // See: https://arduinojson.org/v6/how-to/do-serial-communication-between-two-boards/

          // Add values to the document.
          response_doc["timestamp"] = doc["timestamp"].as<long>();
          response_doc["value"] = doc["value"].as<int>();

        // Get data method.
        } else if (method == "get"){
          // The following document is expected:
          // {"method":"get", "id": 42}

          // Add values to the document.
          response_doc["variable1"] = variable1;
          response_doc["variable2"] = variable2;

        // Set data method.
        } else if (method == "set") {
          // The following document is expected:
          // {"method":"set", "variable2": 42, "variable1": 103, "id": 999}

          // Set variables.
          variable1 = doc["variable1"];
          variable2 = doc["variable2"];
        }

        // Generate the minified JSON and send it to the Serial port.
        serializeJson(response_doc, Serial);
        // Start a new line.
        Serial.println();

      } else {
        // If parsing failed, respond with a JSON document with information.
        StaticJsonDocument<200> err_doc;
        String message = "deserializeJson() returned";
        err_doc["method"] = "error";
        err_doc["data"] = message + err.c_str();
        serializeJson(err_doc, Serial);
        Serial.println();
        serializeJson(doc, Serial);
        Serial.println();

        // Flush all bytes in the "link" serial port buffer.
        while (Serial.available() > 0)
          Serial.read();
      }
    }
    // Parse the next message.
  }
}
