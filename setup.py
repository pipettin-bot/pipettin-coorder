#!/usr/bin/env python
# See: https://stackoverflow.com/a/62983901/11524079
#      https://stackoverflow.com/a/65162874/11524079

import setuptools

with open("README.md", "r", encoding="UTF-8") as fh:
    long_description = fh.read()

if __name__ == "__main__":
    setuptools.setup(
        name='coorder',
        packages=['coorder', 'coorder.types'],
        # packages=setuptools.find_packages()
        long_description=long_description,
        long_description_content_type="text/markdown",
        # https://python-packaging.readthedocs.io/en/latest/dependencies.html
        install_requires=[
            'pyserial',
            'websockets'
        ],
        license='AGPL-3.0-or-later'
    )
