# Generic classes to communicate with devices

This module implements classes to abstract and manage communications with hardware devices, through `JSON-RPC`-like messages.

Main ideas:

1. Code classes that can be used to open, maintain, and close connections through different media (e.g. serial or MQTT).
2. Expose the only one set of methods to interact with the device (i.e. `put` and `get`), letting the user forget about how the connection works.

What is working so far:

- [base_tracker.py](./coorder/base_tracker.py): Base class to abstract message tracking, and implementing `put` and `get` methods.
- [base_executor.py](./coorder/base_executor.py): Executor class, in charge of launching all registered routines (as threads) and coroutines (as asyncio tasks).
- [arduino_pyserial.py](./coorder/types/pyserial.py): Device-specific class using pyserial to drive a PocketPCR [with customized firmare](https://gitlab.com/pipettin-bot/forks/labware/PocketPCR).
    - This is being used in a [`piper` plugin](https://gitlab.com/pipettin-bot/pipettin-piper/-/blob/master/piper/plugins/pocketpcr_serial.py) to drive the PocketPCR from Pipettin.

# Example Scripts

## Read JSON from Pyserial

Defaults to `/dev/ttyACM0` and baudrate `115200`.

The Arduino side of the code should look like these:

- Example firmware: [example-json_rpc-arduino.ino](firmware/example-json_rpc-arduino/example-json_rpc-arduino.ino)
- Customized [PocketPCR](https://gitlab.com/pipettin-bot/forks/labware/PocketPCR/-/tree/master/code/PocketPCR_V2_quite_dev_i_disp) firmware.
- Customized [Colorimeter](https://gitlab.com/pipettin-bot/forks/equipment/photo-colorimeter/-/blob/d5fc9038659573a39a8f146a49db37fbc9cd9e30/Code/firmware_RGBW_decimal_common_anode/firmware_RGBW_decimal_common_anode.ino#L43) firmware.

- Respond to every command with it's ID, or insert an ID equal to `-1` if the command did not have one.

```bash
# Con argumentos default.
python -m coorder.types.pyserial

# Especificando puerto y velocidad serial.
python -m coorder.types.pyserial port="/dev/serial/by-id/usb-Raspberry_Pi_Pico_E6605838833DB138-if00" baudrate=115200
```

## MQTT clients

Using the `aiomqtt` package:

- message sender (MQTT publisher) example: [mqtt-sender.py](./coorder/prototypes/mqtt-sender.py)
- message reader (MQTT subscriber) example: [mqtt-subscriber.py](./coorder/prototypes/mqtt-subscriber.py)

# Safely run a blocking task as an asyncio thread

Useful for running a pyserial connection without blocking the rest: [asyncio-to_thread-test.py](coorder/prototypes/asyncio-to_thread-test.py)

Notes:

- The `to_thread` function in asyncio sends a function to run in another thread. There is no simple way of cancelling the thread task from outside (i.e. from the main co-routine that called it using, for example `gather( to_thread(...) )`). See: <https://stackoverflow.com/questions/71416383/python-asyncio-cancelling-a-to-thread-task-wont-stop-the-thread>
- Python variables are not thread-safe, you must use queues/events/locks to share data with the main co-routines (that live in other threads).

To-do:

- [ ] Find a way to kill the asyncio thread from outside.

# Future work

Variables:

- Capa física: USB, WiFi (IP/redes), Bluetooth.
    - Elegimos USB porque es lo más probable para los aparatitos que manejamos.
    - Estaría bueno dejar lugar para cosas por IP (WiFi, Ethernet, etc.).
- Protocolo: Serial, HTTP, ¿Socket?, ¿RPC?.
- Dispositivo: PocketPCR, Colorímetro, Taperfuga, etc.

Ejecución en threads:

- Cada extensión se ejecuta en un thread separado, se comunica con el programa principal, y también con el dispositivo (via serial USB o HTTP).

Clases a escribir:

- Clase base "Coorder": 
    - Tiene todo lo que hace falta para interactuar con Piper: recibir comandos, esperar respuestas, manejar IDs, timeouts, y excepciones.
    - Tiene métodos "dummy" que hace falta implementar en particular para cada capa física / protocolo / dispositivo.
- Clases "Messenger":
    - Abstracciones para mandar y recibir datos por cierto protocolo / capa física. En principio solo van a hablarle a ArduinoJSON.
    - Cada instancia vive en un thread separado. Hablar con Wlad. ¿Esto debería estar en Coorder?
- Subclases de "Coorder":
    - Tienen lógica particular al dispositivo: ¿qué comandos mandar? ¿cómo procesar las respuestas? etc.
    - Usan una instancia de una clase "Messenger" para mandar y recibir datos.

Tests con simulacion de AVR:

- Armar un test local simulando el hardware en base a esto: <https://github.com/buserror/simavr/tree/master/examples/board_simduino>
- También está simulavr.
