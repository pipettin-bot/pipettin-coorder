# Copyright (C) 2023 Nicolás A. Méndez
#
# This file is part of "coorder".
#
# "coorder" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "coorder" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "coorder". If not, see <https://www.gnu.org/licenses/>.

# pylint: disable=locally-disabled, line-too-long, logging-fstring-interpolation, logging-not-lazy, fixme, multiple-imports, wrong-import-position, wrong-import-order

import queue, logging

class BaseTracker:
    """ Base class to queue pseudo-JSON-RPC messages and responses, and keep track of them in a dictionary by ID.

    Example outgoing message:
        message = {
            "id": 123456,
            "method": "status",
            "data": { argumentos/parametros para el metodo status en el Arduino }
            "date": "fecha a la que se mandó el mensaje"
        }

    Example incoming response:
        response = {
            "id": 123456,
            "data": { response data },
            "date": "fecha a la que se envía la respuesta",
            "error": "optional error property, when something has failed"
        }
    """
    write_queue: queue.Queue
    """Queue containing messages to be sent _to_ the device."""
    read_queue: queue.Queue
    """Queue containing messages received _from_ the device."""
    tracker = dict()
    loglevel: int = logging.DEBUG

    def __init__(self,
                 *args,
                 write_queue:queue.Queue=None,
                 read_queue:queue.Queue=None,
                 verbose:bool=True,
                 logger:logging=None, loglevel:int=None,
                 **kwargs):

        # Send to device queue.
        if write_queue:
            self.write_queue = write_queue
        else:
            self.write_queue = queue.Queue()

        # Received from device queue.
        if read_queue:
            self.read_queue = read_queue
        else:
            self.read_queue = queue.Queue()

        self.verbose: bool = verbose

        self.logging: logging = logger

        if loglevel is not None:
            self.loglevel = loglevel

    def log(self, msg, **kwargs):
        # TODO: use logging, it is thread-safe. Is it share-able between processes though?
        if self.verbose:
            print(msg, **kwargs)
        if self.logging:
            kwargs.setdefault("level", self.loglevel)
            self.logging.log(msg=msg, **kwargs)

    def update(self, cmd_id, key, value):
        # Get the tracker item for the specific ID.
        tracker_item = self.tracker.setdefault(cmd_id, {})
        tracker_item[key] = value

    # PUT & GET queue methods ####
    def put(self, cmd: dict):
        """Put a JSON command into the writer queue"""

        # Get the ID.
        cmd_id = cmd.get("id")
        self.update(cmd_id, "command", cmd)

        # Put the cmd into the queue.
        self.update(cmd_id, "status", "queued")
        self.write_queue.put(cmd)

    def parse_incoming(self):
        """Update the tracker with all messages from the device"""
        self.log('get: parsing reader queue for all new messages.')
        while not self.read_queue.empty():
            item_data = self.read_queue.get()
            item_id = item_data.get("id", None)
            self.update(item_id, "response", item_data)

            # Trigger callbacks.
            item_method = item_data.get("method", None)
            self.note_incoming(item_method, item_data)

    def note_incoming(self, method: str, data: dict):
        """Called on each incoming message, meant to be overriden by subclasses."""

    def get(self, cmd_id=None, response_key="response", pop_id_found=False):
        """Get JSON responses from the reader queue and look for one with the requested id.
        By default, when a response is found, it is popped from the tracker to reduce memory usage.
        """
        # First update the tracker with all messages from the device.
        self.parse_incoming()

        # If an ID was supplied, try finding a response in the tracker.
        tracker_response = None
        if cmd_id:
            self.log(f"get: searching for command with id: '{cmd_id}'")
            tracker_response = self.tracker.get(cmd_id, {}).get(response_key, {})
            if tracker_response:
                self.log(f"get: found response for id='{cmd_id}' with data: {tracker_response}")
                if pop_id_found:
                    self.log(f"Popping entry with id '{cmd_id}'.")
                    # Delete the entry.
                    self.tracker.pop(cmd_id)

        return tracker_response

    def queue_cmd(self, cmd: dict):
        """Send a JSON-RPC through the queue system.
        Returns rapidly if timeout=None. Raises an exception if no response arrived.
        """
        # Send the command.
        self.log(f'queue_cmd: queuing command with cmd={cmd}')
        self.put(cmd)
