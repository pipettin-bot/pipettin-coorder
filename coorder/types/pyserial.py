# Copyright (C) 2023 Nicolás A. Méndez
#
# This file is part of "coorder".
#
# "coorder" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "coorder" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "coorder". If not, see <https://www.gnu.org/licenses/>.

# pylint: disable=locally-disabled, line-too-long, logging-fstring-interpolation, logging-not-lazy, fixme, multiple-imports, wrong-import-position, wrong-import-order

import asyncio, json, time, traceback, threading, multiprocessing, sys, logging
import serial as pyserial
from serial.serialutil import SerialException
from datetime import datetime

# Internal imports
from .base import BaseCommsSync
from ..exceptions import CoorderError

# Flush input buffer
# self.serial.reset_input_buffer()

# Wait until all data is written
# self.serial.flush()

# Flush startup text in serial input
# self.serial.reset_input_buffer()
# Just in case: https://github.com/michaelfranzl/gerbil/blob/e6828fd5a682ec36970de83d38a0bea46b765d8d/interface.py#L69
# self.serial.flushInput()
# self.serial.flushOutput()

class SerialCommunicationSync(BaseCommsSync):
    """A mostly blocking handler for pyserial serial communications, meant to be started as a thread.
    """

    # Serial device property.
    serial: pyserial.Serial = None

    def __init__(self, port, *args, baudrate=9600, launch_type="to_thread", verbose=True, **kwargs):
        """
        Reading material:
        - https://pyserial.readthedocs.io/en/latest/pyserial_api.html#serial.Serial
        """
        self.verbose = verbose
        self.port = port
        self.baudrate = baudrate
        self.serial_message = ""

        # Add methods to the base executor.
        self.launch_type = launch_type
        # NOTE: When launch_type="process" the routines here will be passed the run event and the queues.
        #       Even so, it may not be enough to run correctly.
        self.register_methods()

        # Forward the remaining
        super().__init__(verbose=verbose, *args, **kwargs)

    def connected(self) -> bool:
        result = None
        try:
            result = self.serial.is_open
        except Exception:
            result = False
        return result

    def connect(self):
        result = None
        try:
            if self.connected():
                self.log("connect: already connected.")
                result = True
            elif isinstance(self.serial, pyserial.Serial):
                self.log("connect: not connected. Trying to open the existing serial object.", level = logging.WARNING)
                result = self.open()
                self.log("connect: serial connection re-established.", level = logging.INFO)
            elif self.serial is None:
                try:
                    self.serial = pyserial.Serial(port=self.port,
                                                  baudrate=self.baudrate,
                                                  # NOTE: Possible values for the parameter "timeout" which controls the behavior of read().
                                                  timeout=None, # None: blocking, 0: non-bloking mode, float: blocking with timeout.
                                                  # NOTE: write() is blocking by default, unless write_timeout is set.
                                                  # For possible values refer to the list for timeout above.
                                                  write_timeout=1)
                except SerialException as e:
                    self.log(f"connect: failed to connect with error: {e}\n" + traceback.format_exc(), level=logging.ERROR)
                    result = False
                self.log("connect: serial connection established.", level = logging.INFO)
        except Exception:
            self.log("connect: unhandled failure:\n" + traceback.format_exc(), level = logging.ERROR)
            result = None
        return result

    def open(self):
        result = None
        try:
            self.log("open: Opening connection.")
            self.serial.open()
            # NOTE: Alternatives for this blocking call:
            #       - signal: https://stackoverflow.com/a/2282656
            #       - process: https://stackoverflow.com/a/14924210
        except pyserial.SerialException as e:
            self.log("open: Could not open connection: " + str(e), level = logging.ERROR)
            try:
                self.serial.close()
            except Exception as e2:
                self.log("open: Could not close connection after failure to open: " + str(e2), level = logging.ERROR)
            result = True
        except Exception:
            self.log("open: unhandled failure:\n" + traceback.format_exc(), level = logging.ERROR)
            result = None
        else:
            result = True

        return result

    def register_methods(self):
        """Register (co)routines in the BaseExecutor. They will be run when the start method is called."""
        # Register blocking functions to be run in threads.
        self.register_routine(self.wait_for_connection)

        # TODO: Consider writing a "parser" method that consumes the read_queue.
        # For now nothing reads the messages the reader puts in.
        # NOTE: Not adding it because the idea is that the user implements it.
        # self.register_routine(self.parser)

    # Start/stop methods ####

    async def stop(self, timeout=3):
        """Stop the BaseExecutor and close the serial interface."""
        # Stop everything (do this first as it clears the "run" Event).
        self.log("stop: Handling stop sequence.", level = logging.WARNING)
        await self.stop_executor(timeout=timeout)

        # Cleanup here.
        self.log("stop: closing the serial interface.")
        try:
            if self.connected():
                # Close port "immediately".
                # See: https://pyserial.readthedocs.io/en/latest/pyserial_api.html#serial.Serial.close
                self.serial.close()
        except Exception as e:
            self.log(f"stop: failed to close serial port: {e}\n" + traceback.format_exc(), level = logging.ERROR)
        else:
            self.log("stop: serial port closed.", level = logging.WARNING)

    welcome_cmd = {"method": "welcome", "data": "Hello!", "id": "123456789"}
    def wait_for_connection(self, timeout=3):
        """Non-async serial wait for open method."""
        elapsed = 0
        while not self.connected() and self.run.is_set():
            self.log(f"wait_for_connection: serial is not open, sleeping for {timeout/3} seconds...")
            if elapsed > timeout:
                raise CoorderError("wait_for_connection error: timed out.")
            time.sleep(timeout/4)
            elapsed += timeout/4
        self.log("wait_for_connection: serial connection is online.")
        if self.welcome_cmd:
            self.log(f"wait_for_connection: queuing welcome message: {self.welcome_cmd}")
            self.queue_cmd(self.welcome_cmd)

    def write(self, data):
        """Non-async serial writer method for the writer thread/subprocess method."""
        #if not self.run.is_set():
        #    raise CoorderError("send_data error: coroutines not running.")

        try:
            self.log(f"write: Received data to send to the serial interface: {data}")

            # Wait until the serial interface is connected.
            if not self.connected():
                self.log("write: serial connection not open, running wait_for_connection.")
                self.wait_for_connection()

            # Encode and transmit data.
            self.log(f"write: Sending data to the serial interface: {data}")
            sdata = json.dumps(data)
            self.serial.write(str.encode(sdata))  # encoding="ascii"

            # Wait until all data is written.
            self.serial.flush()

            # Wait until all data is written.
            while self.serial.out_waiting:
                time.sleep(0.1)

        except Exception:
            self.log("write: unhandled failure:\n" + traceback.format_exc(), level = logging.ERROR)
            return False

        return True

    serial_message: str
    def read(self) -> dict:
        # NOTE: The "async" version of pyserial is a bit immature at the moment (2023).
        # Empty dict by default.
        result = {}
        # Check if there is a number of bytes in the receive buffer.
        if self.serial.in_waiting:
            # Reading "serial.in_waiting" bytes instead of "readline" should make this
            # take only a bit of time, instead of blocking the program.
            # message = self.serial.read(size=self.serial.in_waiting).decode().strip("\r\n")
            try:
                # Under that assumption, this reading method will produce complete messages.
                newdata = self.serial.read_until(expected=b"}", size=self.serial.in_waiting).decode().strip("\r\n")
                # self.log("reader: received new data: " + newdata)
                self.serial_message += newdata

            except pyserial.SerialException as e:
                self.log(f"reader: SerialException: {e}\n" + traceback.format_exc(), level = logging.ERROR)

        # Compare opened and closed brackets.
        # NOTE: Messages are expected to be plain JSON objects (i.e. non-nested),
        #       containing a single "{" and a single final "}". Here there is some
        #       support for nested JSON data.
        brackets_ok = self.serial_message.count("{") == self.serial_message.count("}")

        if self.serial_message == "":
            pass
        elif self.serial_message[0] != "{":
            # The message must start with an opening bracket, it is otherwise invalid.
            self.log("reader: discarded malformed JSON message with content: " + self.serial_message)
            # Invalid or not, this will read until a bracket is closed,
            # so it is safe to discard current contents.
            loc = self.serial_message.find("{")
            if loc == -1:
                # If no opening brackets are found, discard the entire message.
                self.log(f"Discarding serial message buffer: {self.serial_message}", level = logging.WARNING)
                self.serial_message = ""
            else:
                # Else, discard the message up to the first opening bracket.
                self.log(f"Discarding serial message buffer: {self.serial_message[:loc]}", level = logging.WARNING)
                self.serial_message = self.serial_message[loc:]
        elif brackets_ok:
            # Deserialize the JSON message into a python dictionary.
            data = json.loads(self.serial_message)
            self.log("reader: Incoming message: " + self.serial_message)
            # Clear serial message buffer.
            self.serial_message = ""
            # Set the result to the data.
            result = data
        # Done!
        return result

    # TEST methods ####
    async def test(self, timeout=2):
        """Test two-way JSON-RPC serial communication with the serial device through the queue system."""

        # Make test command.
        cmd_id, cmd = self.make_ping_cmd()

        # Send command and wait for a response.
        self.log(f'test_connection: testing connection with cmd={cmd}')
        self.queue_cmd(cmd=cmd)

        # Check that a response is received.
        response = await self.get_response(cmd_id=cmd_id, timeout=timeout, fail=False)
        if response:
            self.log(f'test_connection: test success with response={response}')
        else:
            self.log(f'test_connection: test failed with {timeout} second timeout.')

        return response

    @staticmethod
    def make_ping_cmd():
        """Make a ping command with a timestamped ID."""
        cmd_id = f"ping-test-{datetime.now().strftime('%F %T.%f')}"
        cmd = {"method": "ping", "id": cmd_id }
        return cmd_id, cmd

    # MESSAGE HANDLING METHODS ####
    def parser(self):
        """Non-async serial parser method to be launched as a thread or subprocess.
        The idea here is to "do" something with the incoming data. Its a bit silly.
        """

        # Make a dict with methods and their names. This could be anywhere really.
        methods_dict = {"status": self.parse_status}
        # NOTE: Alternatively you could get any method by matching method names to class attributes and beyond.

        # Checks.
        if not self.run.is_set():
            raise CoorderError("parser error: coroutines not running yet.")
        else:
            self.log("parser: coroutine started.")

        # Loop.
        while self.run.is_set():
            if not self.read_queue.empty():
                # A message is available.
                self.log("parser: getting message from queue.")
                data = self.write_queue.get()
                self.log(f"parser: received {data}")
                # Run local procedure call (?)
                method_name = data.get("method", None)
                method_data = data.get("data", None)
                if (method_name is not None) and (method_name in methods_dict.keys()):
                    method = methods_dict[method]
                    self.log(f"parser: calling matched method '{method_name}'.")
                    # NOTE: perhaps it would be ok to send these to threads,
                    # that would allow multiple messages to be processed in parallel,
                    # even if the current method takes too long (for some reason...
                    # or hangs?! god I hope not...).
                    result = method(method_data)
                    self.log(f"parser: result from method '{method_name}': {result}")
                else:
                    self.log(f"parser: unknown method '{method_name}'.")
                # Repeat.
            else:
                time.sleep(0.1)

        self.log("parser: coroutine ended.")

    def parse_status(self, data):
        message = data.get("data")
        self.log(f"parse_status: got status update with text '{message}'")
        return "A result, yay!"






# Module startup functions ####
# TODO: move them to a class.

def start(arduino=None, port='/dev/ttyUSB0', baudrate=9600, launch_type="thread", **kwargs):
    """
    Run SerialCommunicationSync, using built-in threads or processes from BaseExecutor.
    This works but threads be hard to "stop" if they hang.
    The "launch_type" argument is passed to "sync_task" and has three possible values:
        - to_thread: start blocking tasks as threads (using asyncio.to_thread).
        - thread: start blocking tasks as threads.
        - process: start blocking tasks as processes.
    """
    if not arduino:
        arduino = SerialCommunicationSync(port=port, baudrate=baudrate, launch_type=launch_type, **kwargs)
    asyncio.run(arduino.start())

def start_as_thread():
    """
    Run SerialCommunicationSync in a separate Thread.

    This works but the thread can be hard to "stop" if it hangs.
    """
    # Instantiate the arduino controller.
    arduino = SerialCommunicationSync(port='/dev/ttyACM0', baudrate=9600)

    # Create the thread,
    # In computer science, a daemon is a process that runs in the background.
    # Python threading has a more specific meaning for daemon. A daemon thread will shut down immediately when the program exits.
    # See: https://realpython.com/intro-to-python-threading/#starting-a-thread
    thread = threading.Thread(target=start, args=(arduino,), daemon=True)
    thread.start()

    try:
        for i in range(5):
            time.sleep(1)
            print("start_as_thread: slept for", i, "seconds.")
    except KeyboardInterrupt:
        pass

    print("start_as_thread: stopping the coroutines.")
    arduino.run = False
    time.sleep(1)
    print("start_as_thread: joining the thread.")
    thread.join(timeout=3)

def start_as_process():
    """
    Run the "blocking" version of SerialCommunication in a Process.

    This works and it is simple to "force stop".
    """
    # Instantiate the arduino controller.
    arduino = SerialCommunicationSync(port='/dev/ttyACM0', baudrate=9600)

    # Create the process
    proc = multiprocessing.Process(target=start, args=(arduino, ), name = "arduino_pyserial", daemon=True)
    proc.start()

    try:
        for i in range(5):
            time.sleep(1)
            print("start_as_process: slept for", i, "seconds.")
    except KeyboardInterrupt:
        pass

    print("start_as_process: stopping the coroutines.")
    arduino.run = False
    print("start_as_process: joining the process.")
    proc.join(timeout=3)

    # Check the process’s exitcode to determine if it terminated.
    if proc.exitcode is None:
        # https://docs.python.org/3/library/multiprocessing.html#reference
        proc.terminate()

# Run as an independent module.
if __name__ == "__main__":
    # Try running this script from the coorder's parent directory as such:
    #  python -m coorder.arduino_pyserial
    #  python -m coorder.arduino_pyserial port="/tmp/ttyV0" baudrate=9600
    #  python -m coorder.types.pyserial port="/dev/serial/by-id/usb-Raspberry_Pi_Pico_E6605838833DB138-if00" baudrate=115200

    # Parse keyword arguments.
    kw_dict = {}
    for arg in sys.argv[1:]:
        if '=' in arg:
            sep = arg.find('=')
            key, value = arg[:sep], arg[sep + 1:]
            kw_dict[key] = value
        else:
            kw_dict[arg] = True

    # Import the following to use this in another script.
    # from coorder import arduino_pyserial

    # Simple way of starting it.
    # Using to_thread for the blocking serial parts.
    # Choose between "thread" and "to_thread".
    start(launch_type="to_thread", **kw_dict)

    # Start inside a thread.
    # start_as_thread()

    # Start inside a process.
    # NOTE: this has the advantage of being clearly killable, but sharing queues is hard.
    # start_as_process()
