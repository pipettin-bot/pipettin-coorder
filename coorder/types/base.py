# Copyright (C) 2023 Nicolás A. Méndez
#
# This file is part of "coorder".
#
# "coorder" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "coorder" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "coorder". If not, see <https://www.gnu.org/licenses/>.

# pylint: disable=locally-disabled, line-too-long, logging-fstring-interpolation, logging-not-lazy, fixme, multiple-imports, wrong-import-position, wrong-import-order

import asyncio, time, traceback, logging

# Internal imports
from ..base_executor import BaseExecutor
from ..exceptions import CoorderError

class BaseCommsSync(BaseExecutor):
    """A handler for mostly blocking communications, meant to be started as a thread."""

    def __init__(self, *args, launch_type="to_thread", verbose=True, **kwargs):
        """
        Reading material:
        - https://pyserial.readthedocs.io/en/latest/pyserial_api.html#serial.Serial
        """
        self.verbose = verbose

        # Add methods to the base executor.
        self.launch_type = launch_type

        # NOTE: When launch_type="process" the routines here will be passed the run event and the queues.
        #       Even so, it may not be enough to run correctly.
        self.register_base_methods()

        # Forward the remaining
        super().__init__(verbose=verbose, *args, **kwargs)

    # OVERRIDE METHODS ####

    def connected(self) -> bool:
        raise NotImplementedError("You must implement the 'connected' method.")

    def connect(self):
        raise NotImplementedError("You must implement the 'connect' method.")

    async def stop(self, timeout=3):
        """Stop the BaseExecutor and close the serial interface."""
        # Stop everything (do this first as it clears the "run" Event).
        self.log("stop: Handling stop sequence.", level = logging.WARNING)
        await self.stop_executor(timeout=timeout)

        raise NotImplementedError("You must implement the 'stop' method. Remember to call 'stop_executor' first.")

    def write(self, data):
        raise NotImplementedError("You must implement the 'write' method.")

    def read(self) -> dict:
        raise NotImplementedError("You must implement the 'read' method.")

    async def test(self, timeout=2):
        """Test two-way communication with the device through the queue system."""
        raise NotImplementedError("You must implement the 'test' method.")

    def note_incoming(self, method: str, data: dict):
        """Optional override: run callbacks on incoming messages.

        Note that this method is called at a low level, on each call to "get".
        Thus, you are encouraged to run any callbacks by launching a thread or coroutine,
        instead of doing any heavy computation here, as exemplified.
        """
        if method == "status":
            # Using "run_in_loop" will send an awaitable coroutine to the current asyncio loop,
            # without blocking any calls to "get" in the BaseTracker parent class.
            self.run_in_loop(self.dummy_callback, data)

    async def dummy_callback(self, data):
        """Example callback for 'note_incoming' override."""
        if self.verbose:
            print(f"dummy_callback: got 'status' message with data: {data['data']}")
        await asyncio.sleep(2)
        if self.verbose:
            print("dummy_callback: done!")

    # Generic methods ####
    @property
    def is_connected(self):
        return self.connected()

    def register_base_methods(self):
        """Register (co)routines in the BaseExecutor. They will be run when the start method is called."""
        # Register blocking functions to be run in threads.
        self.register_routine(self.opener)
        self.register_routine(self.writer)
        self.register_routine(self.reader)
        self.register_coroutine(self.ping)

        # TODO: Consider writing a "parser" method that consumes the read_queue.
        # For now nothing reads the messages the reader puts in.
        # NOTE: Not adding it because the idea is that the user implements it.
        # self.register_routine(self.parser)

    # Start/stop methods ####
    async def start(self, launch_type=None):
        if launch_type is None:
            launch_type = self.launch_type
        await super().start(launch_type=launch_type)

    async def stop_executor(self, timeout=3):
        """Stop the BaseExecutor and close the serial interface."""
        # Stop everything (do this first as it clears the "run" Event).
        self.log("stop_executor: Stopping the executor.", level = logging.WARNING)
        await super().stop(timeout=timeout)

    # Synchronous methods for threading ####
    def writer(self):
        """Non-async serial writer method to be launched as a thread or subprocess."""

        if not self.run.is_set():
            raise CoorderError("writer error: coroutines not running.")
        else:
            self.log("writer: coroutine started.")

        while self.run.is_set():
            if not self.write_queue.empty():
                self.log("writer: getting message in queue.")

                # Get command.
                data = self.write_queue.get()
                cmd_id = data.get("id", None)

                # Update status.
                if cmd_id:
                    self.update(cmd_id, "status", "sending")

                # Send.
                self.log(f"writer: sending {data}")
                self.write(data)

                # Update status.
                if cmd_id:
                    self.update(cmd_id, "status", "sent")
            else:
                time.sleep(0.1)

        self.log("writer: coroutine ended.")

    def opener(self):
        """Non-async serial open method to be launched as a thread or subprocess."""

        if not self.run.is_set():
            raise CoorderError("opener error: coroutines not running.")
        else:
            self.log("opener: Coroutine started.")

        while self.run.is_set():
            if self.connected():
                self.log("opener: Connection open, sleeping.")
                time.sleep(1)
            elif not self.connect():
                self.log("opener: failed to open connection, sleeping and retrying.")
                time.sleep(0.5)

        self.log("opener: Coroutine ended.")

    def reader(self):
        """Non-async serial reader method to be launched as a thread or subprocess."""

        if not self.run.is_set():
            raise CoorderError("reader error: coroutines not running.")
        else:
            self.log("reader: coroutine started.")

        while self.run.is_set():
            try:
                # Sleep while the serial is closed.
                if not self.connected():
                    time.sleep(0.1)
                    continue

                # Read one message.
                data = self.read()
                if data:
                    self.log(f"reader: Adding incoming message to 'read_queue': {data}")
                    # Add the message to the list.
                    self.read_queue.put(data)

            except Exception as e:
                self.log(f"reader: unhandled failure: {e}\n" + traceback.format_exc(), level = logging.ERROR)

        self.log("reader: coroutine ended.")

    async def ping(self, timeout=1.1, interval=3.1, retries=5):
        """Send ping message, and raise an error if it fails to respond repeatedly."""
        if not self.run.is_set():
            raise CoorderError("ping error: coroutines not running.")

        self.log("ping: coroutine started.")

        retries_left = retries

        while self.run.is_set():
            # Wait for a ping response.
            self.log("ping: queuing ping message and waiting for response.")
            response = await self.test(timeout=timeout)

            if (not response) and (timeout is not None) and (interval is not None):
                if retries_left == 0:
                    raise CoorderError(f"ping: failed to receive response in {retries} retries.")
                else:
                    self.log(f"ping: response not received, retrying ping in {interval} seconds ({retries_left} attempts left).")
                    await asyncio.sleep(interval)
                    retries_left -= 1
            else:
                # Wait before retry.
                self.log(f"ping: test success, sleeping for {interval} seconds.")
                await asyncio.sleep(interval)

        self.log("ping: coroutine ended.")

    # GET RESPONSE METHODS ####
    async def get_response(self, cmd_id, timeout=None, fail=False):
        """Look for a response from the base tracker."""
        self.log(f"get_response: looking for response with cmd_id='{cmd_id}'")
        # Get a response (can be empty).
        response = self.get(cmd_id)
        # Wait for it if not ready.
        elapsed = 0
        while (not response) and (timeout is not None) and self.run.is_set():
            if elapsed > timeout:
                break
            else:
                await asyncio.sleep(timeout/10)
                response = self.get(cmd_id)
                elapsed += timeout/10
        # Check.
        if response:
            self.log(f"get_response: got response for message with data={response}")
        elif fail:
            raise CoorderError(f"get_response: failed to receive response in {timeout} seconds for command with ID: '{cmd_id}'")
        else:
            self.log(f"get_response: failed to receive response in {timeout} seconds for command with ID: '{cmd_id}'")
        # Return the response.
        return response

    def sync_get_response(self, cmd_id, timeout=None, fail=False):
        """Look for a response from the base tracker."""
        # Get a response (can be empty).
        response = self.get(cmd_id)
        # Wait for it if not ready.
        elapsed = 0
        while (not response) and (timeout is not None) and self.run.is_set():
            if elapsed > timeout:
                break
            else:
                time.sleep(timeout/10)
                response = self.get(cmd_id)
                elapsed += timeout/10
        # Check.
        if response:
            self.log(f"sync_get_response: got response for message with data={response}")
        elif fail:
            raise CoorderError(f"sync_get_response: failed to receive response in {timeout} seconds for command with ID: '{cmd_id}'")
        # Return the response.
        return response
