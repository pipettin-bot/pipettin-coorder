# Copyright (C) 2023 Nicolás A. Méndez
#
# This file is part of "coorder".
#
# "coorder" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "coorder" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "coorder". If not, see <https://www.gnu.org/licenses/>.

# pylint: disable=locally-disabled, line-too-long, logging-fstring-interpolation, logging-not-lazy, fixme, multiple-imports, wrong-import-position, wrong-import-order

import asyncio, json, time, traceback, threading, multiprocessing, sys, logging
import websockets
from datetime import datetime

# Internal imports
from .base_async import BaseCommsAsync
from ..exceptions import CoorderError

class WebsocketComms(BaseCommsAsync):
    """An asyncio handler for websockets communications, meant to be started as a coroutine."""

    # Serial device property.
    ws = None

    def __init__(self, uri, *args, launch_type="to_thread", verbose=True, **kwargs):
        """
        Reading material:
        - https://websockets.readthedocs.io/
        """
        self.verbose = verbose
        self.uri = uri
        self.websocket = None

        # Add methods to the base executor.
        self.launch_type = launch_type
        # NOTE: When launch_type="process" the routines here will be passed the run event and the queues.
        #       Even so, it may not be enough to run correctly.
        self.register_methods()

        # Forward the remaining
        super().__init__(verbose=verbose, *args, **kwargs)

    async def connected(self) -> bool:
        return self.websocket is not None and self.websocket.open

    async def connect(self):
        try:
            if await self.connected():
                self.log("connect: already connected.")
                return True

            self.websocket = await websockets.connect(self.uri)
            self.log("connect: connection established.", level = logging.INFO)
            return True

        except Exception as e:
            self.log(f"connect: failed to connect with error: {e}\n" + traceback.format_exc(), level=logging.ERROR)
            return False

    async def disconnect(self):
        if self.websocket:
            await self.websocket.close()
            self.websocket = None
            self.log("disconnect: WebSocket connection closed.")

    def register_methods(self):
        """Register (co)routines in the BaseExecutor. They will be run when the start method is called."""
        # Register blocking functions to be run in threads.
        self.register_routine(self.wait_for_connection)

    # Start/stop methods ####

    async def stop(self, timeout=3):
        """Stop the BaseExecutor and close the WebSocket connection."""
        self.log("stop: Handling stop sequence.", level = logging.WARNING)

        await self.stop_executor(timeout=timeout)

        self.log("stop: closing the WebSocket connection.")
        try:
            if await self.connected():
                await self.disconnect()
        except Exception as e:
            self.log(f"stop: failed to close WebSocket connection: {e}\n" + traceback.format_exc(), level = logging.ERROR)
        else:
            self.log("stop: WebSocket connection closed.", level = logging.WARNING)

    welcome_cmd = {"method": "welcome", "data": "Hello!", "id": "123456789"}
    async def wait_for_connection(self, timeout=6):
        """Non-async websocket wait for open method."""
        elapsed = 0
        while not await self.connected() and self.run.is_set():
            self.log(f"wait_for_connection: websocket is not open, sleeping for {timeout/3} seconds...")
            if elapsed > timeout:
                raise CoorderError("wait_for_connection error: timed out.")
            await asyncio.sleep(timeout/4)
            elapsed += timeout/4
        self.log("wait_for_connection: websocket connection is online.")
        if self.welcome_cmd:
            self.log(f"wait_for_connection: queuing welcome message: {self.welcome_cmd}")
            self.queue_cmd(self.welcome_cmd)

    async def write(self, data):
        """Async WebSocket writer method."""
        try:
            self.log(f"write: Received data to send to the WebSocket: {data}")

            if not await self.connected():
                self.log("write: WebSocket connection not open, running wait_for_connection.")
                await self.wait_for_connection()

            sdata = json.dumps(data)
            await self.websocket.send(sdata)
            self.log(f"write: Sent data to the WebSocket: {sdata}")

        except Exception:
            self.log("write: unhandled failure:\n" + traceback.format_exc(), level = logging.ERROR)
            return False

        return True

    async def read(self) -> dict:
        """Async WebSocket reader method."""
        result = {}
        try:
            if not await self.connected():
                self.log("read: WebSocket connection not open, running wait_for_connection.")
                await self.wait_for_connection()

            message = await self.websocket.recv()
            self.log(f"read: Received message: {message}")
            result = json.loads(message)

        except Exception:
            self.log("read: unhandled failure:\n" + traceback.format_exc(), level = logging.ERROR)

        return result

    # TEST methods ####
    async def test(self, timeout=2):
        """Test two-way JSON-RPC websocket communication with the websocket device through the queue system."""

        # Make test command.
        cmd_id, cmd = self.make_ping_cmd()

        # Send command and wait for a response.
        self.log(f'test_connection: testing connection with cmd={cmd}')
        self.queue_cmd(cmd=cmd)

        # Check that a response is received.
        response = await self.get_response(cmd_id=cmd_id, timeout=timeout, fail=False)
        if response:
            self.log(f'test_connection: test success with response={response}')

        return response

    @staticmethod
    def make_ping_cmd():
        """Make a ping command with a timestamped ID."""
        cmd_id = f"test-{datetime.now().strftime('%F %T.%f')}"
        cmd = {"method": "ping", "id": cmd_id }
        return cmd_id, cmd

# Module startup functions ####
def start(websocket_comms=None, uri='ws://192.168.13.19/ws', launch_type="to_thread", **kwargs):
    """
    Run WebsocketComms, using built-in threads or processes from BaseExecutor.
    """
    if websocket_comms is None:
        websocket_comms = WebsocketComms(uri=uri, launch_type=launch_type, **kwargs)
    asyncio.run(websocket_comms.start())

def start_as_thread():
    """
    Run WebsocketComms in a separate Thread.
    """
    # Instantiate the arduino controller.
    websocket_comms = WebsocketComms(uri='ws://192.168.13.19/ws')

    # Create the thread,
    # In computer science, a daemon is a process that runs in the background.
    # Python threading has a more specific meaning for daemon. A daemon thread will shut down immediately when the program exits.
    # See: https://realpython.com/intro-to-python-threading/#starting-a-thread
    thread = threading.Thread(target=start, args=(websocket_comms,), daemon=True)
    thread.start()

    try:
        for i in range(5):
            time.sleep(1)
            print("start_as_thread: slept for", i, "seconds.")
    except KeyboardInterrupt:
        pass

    print("start_as_thread: stopping the coroutines.")
    websocket_comms.run = False
    time.sleep(1)
    print("start_as_thread: joining the thread.")
    thread.join(timeout=3)

def start_as_process():
    """
    Run the "blocking" version of WebSocketCommunication in a Process.
    """
    websocket_comms = WebsocketComms(uri='ws://localhost:8765')

    proc = multiprocessing.Process(target=start, args=(websocket_comms,), name="websocket_comms", daemon=True)
    proc.start()

    try:
        for i in range(5):
            time.sleep(1)
            print("start_as_process: slept for", i, "seconds.")
    except KeyboardInterrupt:
        pass

    print("start_as_process: stopping the coroutines.")
    websocket_comms.run = False
    print("start_as_process: joining the process.")
    proc.join(timeout=3)

    if proc.exitcode is None:
        proc.terminate()

# Run as an independent module.
if __name__ == "__main__":
    kw_dict = {}
    for arg in sys.argv[1:]:
        if '=' in arg:
            sep = arg.find('=')
            key, value = arg[:sep], arg[sep + 1:]
            kw_dict[key] = value
        else:
            kw_dict[arg] = True

    start(launch_type="to_thread", **kw_dict)
