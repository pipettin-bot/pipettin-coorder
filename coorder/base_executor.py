# Copyright (C) 2023 Nicolás A. Méndez
#
# This file is part of "coorder".
#
# "coorder" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# "coorder" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with "coorder". If not, see <https://www.gnu.org/licenses/>.

# pylint: disable=locally-disabled, line-too-long, logging-fstring-interpolation, logging-not-lazy, fixme, multiple-imports, wrong-import-position, wrong-import-order

import asyncio, traceback, threading, multiprocessing, logging

# Internal imports
from .base_tracker import BaseTracker
from .exceptions import CoorderError

class BaseExecutor(BaseTracker):
    """Task management class."""

    run: multiprocessing.Event
    """Event indicating wether the program is active and should continue, or not and should stop."""

    def __init__(self, *args, run_event=None, verbose=True, **kwargs):
        if run_event is not None:
            self.run = run_event
        else:
            self.run = multiprocessing.Event()
        self.verbose = verbose

        # Forward the remaining arguments.
        super().__init__(verbose=verbose, *args, **kwargs)

    #### START/STOP METHODS SECTION ####
    gather = None
    """Gather 'future' for all tasks."""
    launch_type="to_thread"
    """String defining which method to use for launching blocking tasks in an async context: to_thread thread process"""
    async def start(self, launch_type=None):
        """Start all tasks, threads, and processes.
        The "launch_type" argument is passed to "sync_task" and has three possible values:
            - to_thread: start blocking tasks as threads (using asyncio.to_thread).
            - thread: start blocking tasks as threads.
            - process: start blocking tasks as processes (does not work).
        """
        if launch_type is not None:
            self.launch_type = launch_type
        self.run.set()
        self.log(f"start: Running with launch_type '{launch_type}'.")
        try:
            # Unpack the blocking routines and make tasks/processes/threads with their arguments.
            # NOTE: this populates "self.tasks", "self.threads", or "self.procs", depending on "type".
            _ = [self.sync_task(func, *args, **kwargs) for func, args, kwargs in self.routines]

            # Launch the blocking routines.
            self.launch_routines()

            # Unpack the coroutines and make tasks with their arguments.
            # NOTE: this populates "self.tasks".
            _ = [self.async_task(coro, *args, **kwargs) for coro, args, kwargs in self.coroutines]

            # Gather and await the tasks.
            self.gather = asyncio.gather(*self.tasks)
            await self.gather

        except asyncio.CancelledError:
            self.log("start: CancelledError raised, stopping.", level = logging.WARNING)

        except Exception:
            self.log("start: exception raised, stopping.\n" + traceback.format_exc(), level = logging.ERROR)

        finally:
            # Stop everything.
            self.log("start: Done. Stopping all routines and coroutines.", level = logging.WARNING)
            await self.stop()

    async def stop(self, timeout=3):
        self.log(f"stop: cancelling coroutines and joining/terminating processes (started with type={self.launch_type}).")

        # Signal everything to stop.
        self.run.clear()

        # Cancel asyncio tasks.
        # See: https://docs.python.org/3/library/asyncio-task.html#asyncio.Task.cancel
        self.gather_cancel()

        # Wait for tasks to end, or timeout.
        elapsed = 0
        while not self.gather_done() or not self.procs_terminated() or not self.threads_dead():
            if elapsed > timeout:
                msg = f"stop: timed out, could not stop stuff (tasks_done={self.gather_done()}"
                msg += f", procs_terminated={self.procs_terminated()}"
                msg += f", threads_dead={self.threads_dead()})"
                self.log(msg)
                raise CoorderError(msg)
            await asyncio.sleep(timeout/10)
            elapsed += timeout/10
        if elapsed < timeout:
            msg = f"stop: success! tasks_done={self.gather_done()} procs_terminated={self.procs_terminated()} threads_dead={self.threads_dead()}"
            self.log(msg)

        # NOTE: threads cannot be terminated nor killed.
        # There is no method to "kill" threads.

        # Terminate processes forcefully.
        self.terminate_procs(timeout=timeout)

    #### COROUTINES MANAGEMENT SECTION ####
    coroutines = []
    """List of async functions and arguments to be launched as coroutines at startup."""
    def register_coroutine(self, coroutine, *args, **kwargs):
        # Store everything in a tuple.
        coroutine_and_args = (coroutine, args, kwargs)
        # Add it to the list.
        self.coroutines.append(coroutine_and_args)

    tasks = []
    """Full list of asyncio tasks for the program."""
    def async_task(self, func, *args, **kwargs):
        """Make a task object from a coroutine."""
        task = asyncio.create_task(func(*args, **kwargs))
        self.tasks.append(task)

    def gather_done(self):
        if not self.gather:
            return True
        else:
            return self.gather.done()

    def gather_cancel(self):
        if not self.gather:
            return True
        else:
            if not self.gather.cancelled() and not self.gather.done():
                self.gather.cancel()

    #### ROUTINES MANAGEMENT SECTION ####
    routines = []
    """List of blocking functions and arguments to be launched as a process or thread at startup."""
    def register_routine(self, function, *args, **kwargs):
        # Store everything in a tuple.
        routine_and_args = (function, args, kwargs)
        # Add it to the list.
        self.routines.append(routine_and_args)

    launch_type="to_thread"
    """Routine launch type"""
    def sync_task(self, func, *args, **kwargs):
        """Create a blocking task in a thread (probably non-killable) or process."""

        if self.launch_type == "to_thread":
            coro = asyncio.to_thread(func, *args, **kwargs)
            task = asyncio.create_task(coro)
            self.tasks.append(task)

        elif self.launch_type == "thread":
            self.make_thread(func, *args, **kwargs)

        elif self.launch_type == "process":
            self.make_process(func, *args, **kwargs)

        else:
            raise CoorderError(f"sync_task: Invalid launch_type for launching blocking task: '{self.launch_type}'")

    def launch_routines(self):
        """Start all processes and threads."""
        for throc in self.threads + self.procs:
            throc.start()

    def run_in_loop(self, method: callable, *args, **kwargs):
        """See: https://stackoverflow.com/a/44630895"""
        loop = asyncio.get_event_loop()
        task = loop.create_task(method(*args, **kwargs))
        return task

    #### THREAD MANAGEMENT SECTION ####
    threads = []
    """Full list of Threads for the program."""

    def make_thread(self, func, daemon=True, *args, **kwargs):
        """Create thread for the passed function using python's Threading.
        Reading material:
        - https://docs.python.org/3/library/threading.html#thread-objects
        """
        # Create a thread.
        thrd = threading.Thread(
            target=func,
            args=args, kwargs=kwargs,
            # See: https://docs.python.org/3/library/threading.html#thread-objects
            daemon=daemon)
        # Save it.
        self.threads.append(thrd)
        return thrd

    def join_threads(self, timeout=1):
        for thread in self.threads:
            self.terminate_proc(thread, timeout=timeout)

    def join_thread(self, thread, timeout=0.3):
        """Attempt to wait for a process or terminate it after a timetout."""
        # Join the thread with a timeout.
        thread.join(timeout=timeout)

        # NOTE: threads cannot be killed.

    def threads_dead(self):
        """Iterate over procs and check if all of them have exited."""
        return all(not thrd.is_alive() for thrd in self.threads)

    #### PROCESS MANAGEMENT SECTION ####
    procs = []
    """Full list of Processes for the program."""
    def make_process(self, func, daemon=True, *args, **kwargs):
        """Create process for the passed function using python's Multiprocessing.
        Reading material:
        - https://docs.python.org/3/library/concurrent.futures.html#module-concurrent.futures
        - https://docs.python.org/3/library/asyncio-dev.html#concurrency-and-multithreading
        - https://docs.python.org/3/library/asyncio-eventloop.html#asyncio.loop.run_in_executor
        - https://superfastpython.com/threadpoolexecutor-cancel-task/
        - https://stackoverflow.com/a/29147750
        - https://www.dataleadsfuture.com/combining-multiprocessing-and-asyncio-in-python-for-performance-boosts/
        - https://betterprogramming.pub/run-blocking-functions-asynchronously-in-python-17cd268bb069
        """

        # Create a process.
        proc = multiprocessing.Process(
            target=func,
            args=args, kwargs=kwargs,
            # See: https://docs.python.org/3/library/multiprocessing.html#reference
            daemon=daemon)
        # Save it.
        self.procs.append(proc)
        return proc

    def terminate_procs(self, timeout=1):
        for proc in self.procs:
            self.terminate_proc(proc, timeout=timeout)

    def terminate_proc(self, proc, timeout=0.3):
        """Attempt to wait for a process or terminate it after a timetout."""
        # Join the process with a timeout.
        proc.join(timeout=timeout)

        # Check the process’s exitcode to determine if it terminated.
        # https://docs.python.org/3/library/multiprocessing.html#reference
        if proc.exitcode is None:
            proc.terminate()

    def procs_terminated(self):
        """Iterate over procs and check if all of them have exited."""
        return all([proc.exitcode is not None for proc in self.procs])
