# Copyright (C) 2023 Nicolás A. Méndez
# 
# This file is part of "coorder".
# 
# "coorder" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "coorder" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "coorder". If not, see <https://www.gnu.org/licenses/>.

# FUNCIONA PERO CRASHEA RARO AL TERMINAR, SE CUELGA #

import asyncio
import serial_asyncio  # pip install pyserial-asyncio

class ArduinoSerialCommunication:
    def __init__(self, port, baudrate=115200):
        self.port = port
        self.baudrate = baudrate
        self.reader = None
        self.writer = None
        self.received_messages = []  # List to store received messages
        self.tasks = None

    async def start(self):
        self.run = True
        try:
            reader_task = asyncio.create_task(self.open_connection())
            writer_task = asyncio.create_task(self.receive_data())
            welcome_task = asyncio.create_task(self.wait_for_connection(welcome="Hello!"))

            self.tasks = asyncio.gather(reader_task, writer_task, welcome_task)
            await self.tasks

        except asyncio.KeyboardInterrupt:
            print("KeyboardInterrupt: stopping.")
            await self.stop()
        except asyncio.CancelledError:
            print("CancelledError: Tasks cancelled.")
            self.run = False

    async def stop(self):
        self.run = False
        if self.writer:
            try:
                print(f"La conexión se está cerrando.")
                self.writer.close()
                await asyncio.wait_for(self.writer.wait_closed(), timeout=1)
                print(f"Conexión cerrada con {self.port}")
            except Exception as e:
                print("Uncaught exception: " + str(e))

    async def wait_for_connection(self, welcome=None):
        closed = True
        while True:
            if self.reader:
                closed = self.reader.at_eof()
                if closed:
                    await asyncio.sleep(1)
                else:
                    if welcome:
                        await self.send_data(welcome)
                    return True
            else:
                return False

    async def open_connection(self):
        closed = True
        while self.run:
            if self.writer:
                if self.writer.is_closing():
                    print(f"La conexión se está cerrando.")
                    await asyncio.wait_for(self.writer.wait_closed(), timeout=1)
                    closed = True

            if self.reader:
                closed = self.reader.at_eof()
            
            while closed:  # Check if it is closed.
                print(f"La conexión está cerrada, reconectando...")
                try:
                    self.reader, self.writer = await serial_asyncio.open_serial_connection(
                        url=self.port,
                        baudrate=self.baudrate,
                    )
                    print(f"Conexión establecida con {self.port} a {self.baudrate} bps.")
                    closed = False
                except asyncio.CancelledError:
                    print("Tasks cancelled.")
                    return
                except Exception as e:
                    print(f"Error al abrir la conexión serial: {e}")
                    closed = True
                    print("Esperando 2 segunds antes de reconectar.")
                    await asyncio.sleep(2)  # Intentar reconectar cada 5 segundos
            
            await asyncio.sleep(0.1)  # Chequear cada tanto.

    async def send_data(self, data):
        if self.writer:
            try:
                self.writer.write(data.encode())
                await self.writer.drain()
                print(f"Enviado: {data}")
            except Exception as e:
                print(f"Error al enviar datos: {e}")
        else:
            print(f"No se pudo enviar datos, el emisor no está listo.")

    async def receive_data(self):
        if self.reader:
            try:
                while self.run:
                    data = await self.reader.readuntil(b'\n')
                    decoded_data = data.decode().strip()
                    print(f"Recibido: {decoded_data}")
                    self.received_messages.append(decoded_data)  # Append to the list
            except Exception as e:
                print(f"Error al recibir datos: {e}")

if __name__ == "__main__":
    import sys
    try:
        arduino = ArduinoSerialCommunication('/dev/ttyACM0', baudrate=115200)
        asyncio.run(arduino.start())
    except Exception as e:
        print("Uncaught exception: " + str(e))
    finally:
        print("Done!")
