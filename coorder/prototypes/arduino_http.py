# Copyright (C) 2023 Nicolás A. Méndez
# 
# This file is part of "coorder".
# 
# "coorder" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "coorder" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "coorder". If not, see <https://www.gnu.org/licenses/>.

# UNTESTED #

import asyncio
import httpx  # pip install httpx

class HTTPCommunication:
    def __init__(self, base_url):
        self.base_url = base_url
        self.client = httpx.AsyncClient()  # https://www.python-httpx.org/advanced/
        self.received_messages = []  # List to store received messages
        self.run = False

    async def start(self):
        self.run = True
        try:
            reader_task = asyncio.create_task(self.open_connection())
            writer_task = asyncio.create_task(self.receive_data())
            # welcome_task = asyncio.create_task(self.wait_for_connection(welcome="Hello!"))
            self.tasks = asyncio.gather(reader_task, writer_task)
            await self.tasks
        except asyncio.CancelledError:
            print("CancelledError: Tasks cancelled.")
            self.run = False

    async def open_connection(self):
        """
        Connections are recreated for each pool of messages, 
        there is no equivalent to the serial counterpart, so
        this class only checks wether a GET is possible.
        """
        while self.run:
            try:
                print("Testing open connection...")
                response = await self.client.get(self.base_url)
                if not response.is_success:
                    print("Connection closed.")
            except httpx.HTTPError as e:
                print("Could not open connection " + str(e))
            await asyncio.sleep(1)

    # async def wait_for_connection(self, welcome=None):
    #     if self.run:
    #         if not self.serial.is_open:
    #             await asyncio.sleep(0.1)
    #         elif welcome:
    #             print("Sending welcome.")
    #             await self.send_data(welcome)

    async def stop(self):
        self.run = False
        await self.client.aclose()  # Close transport and proxies.

    async def send_data(self, endpoint, data=None, json=None):
        try:
            url = f"{self.base_url}/{endpoint}"
            # HTTP POST requests supply additional data from the client (browser) to the server in the message body.
            # In contrast, GET requests include all required data in the URL.
            # https://stackoverflow.com/a/3477374 
            # ... Essentially GET is used to retrieve remote data, and POST is used to insert/update remote data.
            response = await self.client.post(url, data=data, json=json)
            response.raise_for_status()
            return response.text
        except httpx.HTTPError as e:
            print(f"Error in HTTP request to {url}: {e}")
            return None

    async def receive_data(self):
        while self.run:
            try:
                response_text = await self.send_request("receive")
                if response_text:
                    print(f"Received: {response_text}")
                    self.received_messages.append(response_text)
            except httpx.HTTPError as e:
                print(f"HTTPError while receiving data: {e}")
            except Exception as e:
                print(f"Unexpected error while receiving data: {e}")
            await asyncio.sleep(1)  # Adjust the interval as needed

# Example of usage
if __name__ == "__main__":
    try:
        arduino = HTTPCommunication(base_url='http://esp32_ip_address')
        asyncio.run(arduino.start())
    except Exception as e:
        print("Uncaught exception: " + str(e))
    finally:
        asyncio.run(arduino.stop())
        print("Done!")
