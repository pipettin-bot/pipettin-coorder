# Copyright (C) 2023 Nicolás A. Méndez
# 
# This file is part of "coorder".
# 
# "coorder" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "coorder" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "coorder". If not, see <https://www.gnu.org/licenses/>.

import asyncio, time, threading, sys

import queue, multiprocessing

from pprint import pprint

import traceback


class MyClass:
    a = 0
    top = 4
    q = queue.Queue()
    run = threading.Event()
    run.set()
    task = None
    #process = None
    
    def blocking_io(self):
        # get the current process
        #self.process = multiprocessing.current_process()
        #print("Current process:", self.process, dir(self.process))
        
        print(f"start blocking_io at {time.strftime('%X')}")
        # Note that time.sleep() can be replaced with any blocking
        # IO-bound operation, such as file operations.
        while self.a <= self.top:
            if not self.run.is_set():
                print(f"blocking_io: run flag unset, exiting.")
                return
            
            print(f"blocking_io: a={self.a} adding 1 to queue")
            self.q.put(1)
            time.sleep(2)
        print(f"blocking_io complete at {time.strftime('%X')}")

    async def printcount(self):
        print(f"start non_blocking_coro at {time.strftime('%X')}")
        try:
            while self.a <= self.top:
                while not self.q.empty():
                    self.a += self.q.get()
                    print(f"printcount: current value of a={self.a}")
                    await asyncio.sleep(1)
                
                # TEST: uncomment the following to try kill the thread if something happens here.
                # if self.a == 3:
                #    raise CoorderError("printcount: Terminating")
                
                await asyncio.sleep(0.1)
        
        except asyncio.CancelledError as e:
            pass
        print(f"non_blocking_coro complete at {time.strftime('%X')}")
    
    # NOTE: the watchdog does not seem necessary if the thread is started as a task (see below).
    # NOTE: i'll let it be in case something goes wrong.
    async def watchdog(self):
        try:
            while True:
                await asyncio.sleep(1)
        except asyncio.CancelledError as e:
            print(f"watchdog: triggered with canncelled error and message: '{e}'")
            pprint(self.task)
            
            # NOTE: tried to terminate the thread externally, but it did not work. I got:
            # self._popen.terminate()
            #    ^^^^^^^^^^^^^^^^^^^^^
            # AttributeError: 'NoneType' object has no attribute 'terminate'
            #if self.process is not None:
            #    print("Current process:", self.process, dir(self.process))
            #    # See: https://superfastpython.com/kill-a-thread-in-python/#Killing_a_Process_With_Code
            #    # terminate the process (sigterm)
            #    # self.process.terminate()
            #    # kill the process (sigkill)
            #    self.process.kill()
                
            self.run.clear()
            

    async def start(self):
        print(f"started main at {time.strftime('%X')}")
        
        try:
            
            # See: https://superfastpython.com/asyncio-blocking-tasks/#Example_of_Running_IO-Bound_Task_in_Asyncio_with_to_thread
            # Create a coroutine for the blocking task.
            coro = asyncio.to_thread(self.blocking_io)
            # Schedule the task.
            self.task = asyncio.create_task(coro)
            # Allow the scheduled task to start.
            #await asyncio.sleep(0)
            # await the task.
            #await self.task
            
            await asyncio.gather(
                self.task, # NOTE: the task seems to run even if it is not gathered here. Maybe any await will let it run.
                # asyncio.to_thread(self.blocking_io),  # NOTE: replaced by a task instance above.
                
                # NOTE: the watchdog does not seem necessary if the thread is started as a task (see below).
                # NOTE: i'll let it be in case something goes wrong, and I need to end the thread.
                self.watchdog(),
                
                # Start the coroutine that waits for queue items from the thread.
                self.printcount()
                )
            
            # Wait for the task here (hoping to prevent "never awaited" errors).
            #await self.task
            # NOTE: not using "await task" here, as I fear the threads can hang for some reason... leaving room for a timeout.
            while not self.task.done() and not self.task.cancelled():
                await asyncio.sleep(1)
            
        except Exception as e:
            print("main: got exception with message: " + str(e))
            
            print(traceback.format_exc())
            # print(sys.exc_info()[2])

            self.run.clear()
            

        print(f"finished main at {time.strftime('%X')}")


if __name__ == "__main__":
    myclass = MyClass()
    try:
        # Regular way of starting it.
        asyncio.run(myclass.start())
    except KeyboardInterrupt as e:
        print("Program interrupted.")

    print("Done!")

# Expected output:
#
# started main at 20:41:44
# start blocking_io at 20:41:44
# blocking_io: a=0 adding 1 to queue
# start non_blocking_coro at 20:41:44
# printcount: current value of a=1
# blocking_io: a=1 adding 1 to queue
# printcount: current value of a=2

