# Copyright (C) 2023 Nicolás A. Méndez
# 
# This file is part of "coorder".
# 
# "coorder" is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# 
# "coorder" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License along with "coorder". If not, see <https://www.gnu.org/licenses/>.

import asyncio
import aiomqtt
import json

# Connecting to the broker
# https://sbtinstruments.github.io/aiomqtt/connecting-to-the-broker.html

class mqttClient:
    # The Client context is designed to be reusable (but not reentrant). 
    client = aiomqtt.Client("localhost", port=1883)  # The default mosquitto port is 1883.
    interval = 3  # Seconds
    
    # Reconnection
    # https://sbtinstruments.github.io/aiomqtt/reconnection.html#reconnection
    async def connect(self):
        while True:
            try:
                async with aiomqtt.Client("localhost", port=1883) as client:
                    async with client.messages() as messages:
                        await client.subscribe("humidity/#")
                        print("Subscribed to topic. Waiting for messages...")
                        async for message in messages:
                            data = message.payload.decode()
                            try:
                                data = json.loads(data)
                            except:
                                pass
                            # https://stackoverflow.com/a/5008854
                            print(f"Received data with type '{type(data).__name__}' and data: ", data)
            except aiomqtt.MqttError:
                print(f"Connection lost ({e}); Reconnecting in {self.interval} seconds ...")
                await asyncio.sleep(self.interval)

    def run(self):
        asyncio.run(self.connect())


if __name__ == "__main__":
    client = mqttClient()
    client.run()
